const {Schema, model} = require('mongoose');

const User = new Schema( {
    username: {type: String, unique: true, required: true},
    password: {type: String, required: true},
    // email: {type: String, required:true, max: 255, min: 6},
    // age: {type: Number, min: 1, max: 3},
    roles: [{type: String, ref: 'Role'}]
})

module.exports = model('User', User)